package main

// Implementation of RFC1878 as an API accessible via HTTP
// This API will reply useful information for a given Classless Inter-Domain
// Routing (cidr) name.
// An API call could look like this <server-ip>:8080/cidr/{cidr}

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"

	cidr "gitlab.com/olzemal/netcalc-go/cidr"
	snm "gitlab.com/olzemal/netcalc-go/snm"
)

type response struct {
	Success bool    `json:"success"`
	Message string  `json:"message"`
	Result  *result `json:"result"`
}

type result struct {
	Cidr       string `json:"cidr"`
	MaxAddrs   int    `json:"maxaddrs"`
	MaxHosts   int    `json:"maxhosts"`
	Subnetmask string `json:"subnetmask"`
	Class      string `json:"class"`
}

func route(w http.ResponseWriter, r *http.Request) {
	// Set Content type to JSON
	w.Header().Set("content-type", "application/json")

	// Log every Request
	log.Println(
		r.RemoteAddr,
		r.Method,
		r.RequestURI,
		r.UserAgent())

	path := strings.Split(r.RequestURI, "/")
	if len(path) < 3 {
		w.WriteHeader(403)
		errorHandler(w, r, fmt.Errorf("Invalid URI: \"%s\"", r.RequestURI))
		return
	}
	switch path[1] {
	case "cidr":
		if len(path) < 4 {
			w.WriteHeader(400)
			errorHandler(w, r, fmt.Errorf("Missing argument for /cidr"))
			return
		}
		i, err := strconv.Atoi(path[2])
		if err != nil {
			w.WriteHeader(400)
			errorHandler(w, r, fmt.Errorf(
				"Not a valid cidr: \"%s\"", path[2]))
			return
		}
		c, err := cidr.FromInt(i)
		if err != nil {
			w.WriteHeader(400)
			errorHandler(w, r, fmt.Errorf(
				"Not a valid cidr: \"%s\"", path[2]))
			return
		}
		cidrHandler(w, r, c)
	case "snm":
		snm, err := snm.FromString(path[2])
		if err != nil {
			w.WriteHeader(400)
			errorHandler(w, r, fmt.Errorf(
				"Not a valid subnetmask: \"%s\"", path[2]))
			return
		}
		c, _ := cidr.FromSubnetmask(snm)
		cidrHandler(w, r, c)
	default:
		return
	}
}

func cidrHandler(w http.ResponseWriter, r *http.Request, c *cidr.CIDR) {
	s, err := snm.FromCIDR(c)
	if err != nil {
		errorHandler(w, r, err)
		return
	}
	result := &result{
		c.String(),
		c.MaxAddresses(),
		c.MaxHosts(),
		s.String(),
		c.Class(),
	}
	response := &response{
		true,
		"",
		result,
	}
	json, err := json.MarshalIndent(response, "", "  ")
	if err != nil {
		log.Fatalf("json.MarshalIndent: %v", err)
	}
	fmt.Fprintf(w, "%s\n", string(json))
}

func errorHandler(w http.ResponseWriter, r *http.Request, err error) {
	log.Printf("ERROR: %s %s %s %s: %s",
		r.RemoteAddr,
		r.Method,
		r.RequestURI,
		r.UserAgent(),
		err.Error())
	response := &response{
		false,
		err.Error(),
		nil,
	}
	json, err := json.MarshalIndent(response, "", "  ")
	if err != nil {
		log.Fatalf("json.MarshalIndent: %v", err)
	}
	fmt.Fprintf(w, "%s\n", string(json))
}

func main() {
	http.HandleFunc("/", route)
	http.ListenAndServe(":8080", nil)
}
