package snm

import "testing"

func TestStringer(t *testing.T) {
	strs := []string{"255.255.128.0", "255.255.255.0"}
	for _, str := range strs {
		s, _ := FromString(str)
		got := s.String()
		if got != str {
			t.Errorf("got \"%s\"; want \"%s\"", got, str)
		}
	}
}
