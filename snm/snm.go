package snm

import (
	"encoding/binary"
	"fmt"
	"strconv"
	"strings"
)

// Subnetmask holds a valid subnetmask
type Subnetmask struct {
	val [4]byte
}

type cidr interface {
	Int() int
}

// FromString creates a valid subnetmask object from a given string
func FromString(snm string) (*Subnetmask, error) {
	invalid := fmt.Errorf("Invalid subnetmask: %s", snm)
	oct := strings.Split(snm, ".")
	if len(oct) != 4 {
		return nil, invalid
	}
	mask := 0
	for i, s := range oct {
		v, err := strconv.Atoi(s)
		if err != nil {
			return nil, invalid
		}
		isFirstIteration := i == 0
		if !isFirstIteration {
			mask = mask << 8
		}
		mask = mask | v
	}

	// FIXME: This should be more efficient
	for cidr := 0; cidr <= 32; cidr++ {
		possibleMask := (0xffffffff << cidr) & 0xffffffff
		if mask == possibleMask {
			var val [4]byte
			binary.BigEndian.PutUint32(val[0:4], uint32(mask))
			return &Subnetmask{val}, nil
		}
	}
	return nil, invalid
}

// FromCIDR creates a valid subnetmask object from a given CIDR object
// The error handling is still done with FromString
func FromCIDR(c cidr) (*Subnetmask, error) {
	bin := (0xffffffff << (32 - c.Int())) & 0xffffffff

	var oct [4]byte
	binary.BigEndian.PutUint32(oct[0:4], uint32(bin))

	s := fmt.Sprintf("%d.%d.%d.%d", oct[0], oct[1], oct[2], oct[3])
	return FromString(s)
}

// String returns the subnetmask as string
func (snm Subnetmask) String() string {
	str := ""
	for i, oct := range snm.val {
		str += strconv.Itoa(int(oct))
		if i < len(snm.val)-1 {
			str += "."
		}
	}
	return str
}

// Bytes returns the subnetmask as a byte array
func (snm Subnetmask) Bytes() [4]byte {
	return snm.val
}
