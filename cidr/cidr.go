package cidr

import (
	"fmt"
	"math"
	"math/bits"
	"strconv"
)

// CIDR holds a valid Classless Interdomain Route
type CIDR struct {
	val int
}

type subnetmask interface {
	Bytes() [4]byte
}

// FromInt creates a new valid CIDR
func FromInt(cidr int) (*CIDR, error) {
	valid := cidr > 0 && cidr <= 32
	if valid {
		return &CIDR{cidr}, nil
	}
	return nil, fmt.Errorf("Invalid value for cidr: %d", cidr)
}

// FromSubnetmask creates a new valid CIDR from a given subnetmask
// Error handling is still done by FromInt
func FromSubnetmask(s subnetmask) (*CIDR, error) {
	ones := 0
	for _, oct := range s.Bytes() {
		ones += bits.OnesCount(uint(oct))
	}
	return FromInt(int(ones))
}

// String returns the CIDR as string
func (c CIDR) String() string {
	return strconv.Itoa(c.val)
}

// Int returns the CIDR as integer
func (c CIDR) Int() int {
	return c.val
}

// MaxAddresses calculates the total amount of addresses for a CIDR, including
// reserved addresses (network and broadcast address)
func (c CIDR) MaxAddresses() int {
	clientBits := 32 - c.Int()
	return int(math.Pow(2, float64(clientBits)))
}

// MaxHosts returns the total amount of usable addresses for a CIDR, excluding
// reserved addresses
func (c CIDR) MaxHosts() int {
	hosts := c.MaxAddresses() - 2
	if hosts > 0 {
		return hosts
	}
	return 0
}

// Class returns the class of a given CIDR, ignoring edge cases such as
// multicasts
func (c CIDR) Class() string {
	if c.val == 32 {
		return "Single host route"
	}
	switch c.val/8 + 1 {
	case 1:
		// CIDR is between 1 and 8
		return "A"
	case 2:
		// CIDR is between 9 and 16
		return "B"
	default:
		// CIDR is between 17 and 24
		return "C"
	}
}
