package cidr

import "testing"

func TestFromInt(t *testing.T) {
	invalid := CIDR{-1}
	tests := []struct {
		input int
		want  CIDR
	}{
		{17, CIDR{17}},
		{1, CIDR{1}},
		{33, invalid},
		{0, invalid},
	}
	for _, test := range tests {
		got, err := FromInt(test.input)
		if err != nil {
			if got != nil {
				t.Errorf("got %v; want: %v", got, nil)
			}
			return
		}
		if *got != test.want {
			t.Errorf("got: %v; want: %v", got, test.want)
		}
	}
}

func TestStringer(t *testing.T) {
	tests := []struct {
		input int
		want  string
	}{
		{16, "16"},
		{27, "27"},
		{33, "error"},
	}
	for _, test := range tests {
		c, err := FromInt(test.input)
		if err == nil && test.want == "error" {
			t.Errorf("Expected an error when trying for %v", test.input)
		}
		if err != nil {
			if test.want == "error" {
				return
			}
			t.Errorf("Got unexpected error %v when trying for %v",
				err.Error(), test.input)
		}

		got := c.String()
		if got != test.want {
			t.Errorf("got: %v; want: %v", got, test.want)
		}
	}
}
